# About NodeJS of Docker
# 
# Version:1.0.0

FROM centos:7
MAINTAINER Dubu Qingfeng <1135326346@qq.com>

RUN yum -y update; yum clean all

# install the epel

RUN yum -y install epel-release; yum clean all

# install the nodejs and npm

RUN yum -y install \
      nodejs \
      git \
      npm ; \
    yum -y clean all

# Download latest version of Agar-clone into /app
RUN rm -fr /app && git clone --depth=1 https://github.com/huytd/agar.io-clone.git /app

ADD . /src

RUN cd /app

RUN npm install express socket.io serve-favicon config morgan async node-minify \
    handlebars lodash walk pm2

RUN npm install gulp gulp-babel gulp-jshint gulp-nodemon gulp-uglify gulp-util gulp-mocha webpack-stream chai sat babel

VOLUME /app

EXPOSE 1337 3000

#CMD ["node", "/src/app.js"]
CMD ["npm", "start"]
